
var getData = require('../api/api.js');
const fs = require('fs');
var ValoresDolar = [];
var ValoresEuro = [];
var ValoresT = [];
var resultado;

var ValoresDolarM = [];
var ValoresEuroM = [];
var ValoresTM = [];

var resultadoEuro;
var resultadoT;
var resultadoM;

var resultadoDM;
var resultadoEM;
var resultadoTM;
var PromedioFinalDolar;
var PromedioFinalEuro;
var PromedioFinalT;
function actualizar() {

	var hoy = new Date();
	var fecha = hoy.getFullYear() + "" + check((hoy.getMonth() + 1)) + "" + check(hoy.getDate()) + "" + hoy.getHours() + "" + check(hoy.getMinutes()) + "" + check(hoy.getSeconds());
	var fechatext = fecha.toString();
	function check(i) {
		if (i < 10) {
			i = "0" + i;
		}
		return i;
	}

	return new Promise((resolve, reject) => {

		getData().then(function (datos) {

			let jsonFiltrado = {
				version: datos.version,
				autor: datos.autor,
				fecha: datos.fecha,
				dolar: {
					codigo: datos.dolar.codigo,
					nombre: datos.dolar.nombre,
					unidad_medida: datos.dolar.unidad_medida,
					fecha: datos.dolar.fecha,
					valor: datos.dolar.valor
				},
				euro: {
					codigo: datos.euro.codigo,
					nombre: datos.euro.nombre,
					unidad_medida: datos.euro.unidad_medida,
					fecha: datos.euro.fecha,
					valor: datos.euro.valor
				},
				tasa_desempleo: {
					codigo: datos.tasa_desempleo.codigo,
					nombre: datos.tasa_desempleo.nombre,
					unidad_medida: datos.tasa_desempleo.unidad_medida,
					fecha: datos.tasa_desempleo.fecha,
					valor: datos.tasa_desempleo.valor
				}
			};


			fs.writeFileSync("datos/datos.ind " + fechatext, JSON.stringify(jsonFiltrado));

			resolve("Datos actualizados!")
		});
	});
}

function promedioDolar() {

	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents);
			contents.forEach(element => {

				const getFile = fileName => {
					return new Promise((resolve, reject) => {
						fs.readFile(fileName, (err, data) => {
							if (err) {
								return reject(err);
								console.log(err);
							}

							resolve(data);
						});

					});
				}

				getFile('datos/' + element)

					.then(JSON.parse)
					.then(datos => {

						resultado = datos.dolar.valor;

						ValoresDolar.push(resultado);

						var sum = ValoresDolar.reduce((previous, current) => current += previous);

						PromedioFinalDolar = sum / ValoresDolar.length;

						return PromedioFinalDolar;

					})
					.catch(console.error);
				resolve(Math.trunc(PromedioFinalDolar))
			});

		});

	});


}

function promedioEuro() {

	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents);
			contents.forEach(element => {

				const getFile = fileName => {
					return new Promise((resolve, reject) => {
						fs.readFile(fileName, (err, data) => {
							if (err) {
								return reject(err);
								console.log(err);
							}

							resolve(data);
						});

					});
				}

				getFile('datos/' + element)

					.then(JSON.parse)
					.then(datos => {

						resultadoEuro = datos.euro.valor;

						ValoresEuro.push(resultadoEuro);

						var sum = ValoresEuro.reduce((previous, current) => current += previous);

						PromedioFinalEuro = sum / ValoresEuro.length;

						return PromedioFinalEuro;

					})
					.catch(console.error);
				resolve(Math.trunc(PromedioFinalEuro))
			});

		});

	});


}

function promedioT() {

	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents);
			contents.forEach(element => {

				const getFile = fileName => {
					return new Promise((resolve, reject) => {
						fs.readFile(fileName, (err, data) => {
							if (err) {
								return reject(err);
								console.log(err);
							}

							resolve(data);
						});

					});
				}

				getFile('datos/' + element)

					.then(JSON.parse)
					.then(datos => {

						resultadoT = datos.tasa_desempleo.valor;

						ValoresT.push(resultadoT);

						var sum = ValoresT.reduce((previous, current) => current += previous);

						PromedioFinalT = sum / ValoresT.length;

						return PromedioFinalT;

					})
					.catch(console.error);
				resolve(Math.trunc(PromedioFinalT))
			});

		});

	});


}

function MasActualDolar() {
	var masActualD;


	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents[contents.length-1]);


			const getFile = fileName => {
				return new Promise((resolve, reject) => {
					fs.readFile(fileName, (err, data) => {
						if (err) {
							return reject(err);
							console.log(err);
						}

						resolve(data);
					});

				});
			}

			getFile('datos/' + contents[contents.length - 1])

				.then(JSON.parse)
				.then(datos => {

					masActualD = datos.dolar.valor;





					resolve(masActualD);

				})
				.catch(console.error);



		});

	});


}

function MasActualEuro() {
	var masActualE;


	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents[contents.length-1]);


			const getFile = fileName => {
				return new Promise((resolve, reject) => {
					fs.readFile(fileName, (err, data) => {
						if (err) {
							return reject(err);
							console.log(err);
						}

						resolve(data);
					});

				});
			}

			getFile('datos/' + contents[contents.length - 1])

				.then(JSON.parse)
				.then(datos => {

					masActualE = datos.euro.valor;





					resolve(masActualE);

				})
				.catch(console.error);



		});

	});


}

function MasActualT() {
	var masActualT;
	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents[contents.length-1]);


			const getFile = fileName => {
				return new Promise((resolve, reject) => {
					fs.readFile(fileName, (err, data) => {
						if (err) {
							return reject(err);
							console.log(err);
						}

						resolve(data);
					});

				});
			}

			getFile('datos/' + contents[contents.length - 1])

				.then(JSON.parse)
				.then(datos => {

					masActualT = datos.tasa_desempleo.valor;

					resolve(masActualT);

				})
				.catch(console.error);

		});

	});


}

function MinimoHistorico() {
var valorMinHD ;
	return new Promise((resolve, reject) => {

		function run(gen) {
			var iter = gen((err, data) => {
				if (err) { iter.throw(err); }

				return iter.next(data);
			});

			iter.next();
		}

		const dirPath = 'datos';
		run(function* (resume) {

			var contents = yield fs.readdir(dirPath, resume);
			//console.log(contents);
			contents.forEach(element => {

				const getFile = fileName => {
					return new Promise((resolve, reject) => {
						fs.readFile(fileName, (err, data) => {
							if (err) {
								return reject(err);
								console.log(err);
							}

							resolve(data);
						});

					});
				}

				getFile('datos/' + element)

					.then(JSON.parse)
					.then(datos => {

						resultadoDM = datos.dolar.valor;
						

						ValoresDolarM.push(resultadoDM);
						
						 valorMinHD = Math.max(ValoresDolarM);
						

						

						return valorMinHD;

					})
					.catch(console.error);
				resolve(valorMinHD)
			});

		});

	});


}

module.exports = { actualizar, promedioDolar, promedioEuro, promedioT, MasActualDolar, MasActualEuro, MasActualT,MinimoHistorico }
