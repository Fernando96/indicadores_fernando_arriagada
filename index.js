const readline = require('readline');

var getData = require('./opciones/opciones.js');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});



function menu() {

    console.log('MENU:');
    console.log('1.- Actualizar indicadores.');
    console.log('2.- Promediar.');
    console.log('3.- Mostrar valor más actual.');
    console.log('4.- Mostrar mínimo histórico.');
    console.log('5.- Mostrar máximo histórico.');
    console.log('6.- Salir.');

    lector.question('Escriba su opcion: ', opcion => {
        switch (opcion) {
            case '1':
                console.log('Actualizacion de datos');
                getData.actualizar().then(menus => {
                    console.log(menus);
                    menu();
                });

                break;
            case '2':
                console.log('Promedios');
              
                SubMenuPromedio();

                break;
            case '3':
                console.log("Valor más actual");
                SubMenuMasActual();
                break;
            case '4':
                console.log("Mínimo Historico");
                getData.MinimoHistorico().then(mh => {
                    console.log(mh);
                    menu();
                });
                break;
            case '5':
                console.log("Máximo Histórico");
                menu();
                break;
            case '6':
                console.log('');
                lector.close();
                process.exit(0);
                break;
            default:
                console.log("Opción incorrecta");
                menu();
                break;
        }
    });

}

function SubMenuPromedio() {

    console.log('\nMENU PROMEDIOS:');
    console.log('PROMEDIAR:');
    console.log('1.- Dólar.');
    console.log('2.- Euro.');
    console.log('3.- Tasa de Desempleo.');
    console.log('4.- Volver.');

    lector.question('Escriba su opcion: ', opcion => {
        switch (opcion) {
            case '1':
                console.log('Dólar');
                getData.promedioDolar().then(promedio => {
                    console.log(promedio);
                    SubMenuPromedio();
                });
                break;
            case '2':
                console.log('Euro');
                getData.promedioEuro().then(promedio => {
                    console.log(promedio);
                    SubMenuPromedio();
                });
                break;
            case '3':
                console.log("Tasa de Desempleo");
                getData.promedioT().then(promedio => {
                    console.log(promedio+"%");
                    SubMenuPromedio();
                });
                break;
            case '4':
                console.log('');
                menu();
                break;
            default:
                console.log("Opción incorrecta");
                SubMenuPromedio();
                break;
        }
    });

}

function SubMenuMasActual() {

    console.log('\nMENU VALOR MÁS ACTUAL:');
    console.log('1.- Dólar.');
    console.log('2.- Euro.');
    console.log('3.- Tasa de Desempleo.');
    console.log('4.- Volver.');

    lector.question('Escriba su opcion: ', opcion => {
        switch (opcion) {
            case '1':
                console.log('Dólar');
                getData.MasActualDolar().then(actual => {
                    console.log(actual);
                    SubMenuPromedio();
                });
                break;
            case '2':
                console.log('Euro');
                getData.MasActualEuro().then(actual => {
                    console.log(actual);
                    SubMenuPromedio();
                });
                break;
            case '3':
                console.log("Tasa de Desempleo");
                getData.MasActualT().then(actual => {
                    console.log(actual);
                    SubMenuPromedio();
                });
                break;
            case '4':
                console.log('');
                menu();
                break;
            default:
                console.log("Opción incorrecta");
                SubMenuMasActual();
                break;
        }
    });

}



menu();

module.exports = menu;
